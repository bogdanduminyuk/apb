import datetime as dt
import os
import re
import sqlite3
import time

import requests
from bs4 import BeautifulSoup as bs

DATABASE = os.path.abspath("./cache/currencies.db")

HEADERS = {
    "accept": "text/html, */*; q=0.01",
    "accept-encoding": "gzip, deflate, br",
    "accept-language": "ru,ru-RU;q=0.9,en-US;q=0.8,en;q=0.7,uk;q=0.6",
    "referer": "https://www.agroprombank.com/info/hisrates.html",
    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36",
    "x-requested-with": "XMLHttpRequest",
    "path": "/histrates.php?type=ib&date=30.09.2020",
    "authority": "www.agroprombank.com",
    "scheme": "https"
}


def __bs_preprocess__(html):
    """remove distracting whitespaces and newline characters"""
    pat = re.compile('(^[\s]+)|([\s]+$)', re.MULTILINE)
    html = re.sub(pat, '', html)       # remove leading and trailing whitespaces
    html = re.sub('\n', ' ', html)     # convert newlines to spaces
                                       # this preserves newline delimiters
    html = re.sub('[\s]+<', '<', html) # remove whitespaces before opening tags
    html = re.sub('>[\s]+', '>', html) # remove whitespaces after closing tags
    return html


def get_data(date):
    params = {
        "type": "ib",
        "date": date.strftime("%d.%m.%Y"),  
    }
    
    response = requests.get(
        "https://www.agroprombank.com/histrates.php", 
        params=params,
        headers=HEADERS
    )
        
    if response.ok:
        return response.text

    else:
        raise ValueError
    


N = 10
now = dt.datetime.now()

conn = sqlite3.connect(DATABASE)

conn.execute("""
    CREATE TABLE IF NOT EXISTS currencies_list (
        currency_id TEXT PRIMARY KEY,
        currency_name TEXT UNIQUE
    );
""")

conn.execute("""
    CREATE TABLE IF NOT EXISTS currencies_values (
        date_id TEXT PRIMARY KEY,
        currency_id TEXT NOT NULL,
        buy_value REAl NOT NULL,
        sell_value REAL NOT NULL,
        FOREIGN KEY (currency_id) REFERENCES currencies_list (currency_id)
    );
""")


c = conn.cursor()



for date in (now - dt.timedelta(n) for n in range(N)):
    c.execute("SELECT date_id FROM currencies_values WHERE DATE(date_id) == DATE('{}')".format(date))
    
    if not c.fetchall():
        html = get_data(date)
        html = __bs_preprocess__(html)
        soup = bs(html, "html.parser")
        
        tr = soup.find_all("tr")
        
        for row in tr[1:]:
            children = [child for child in row.children]
            
            try:
                c.execute("""INSERT INTO currencies_list VALUES ('{}', '{}')"""
                    .format(children[0].text, children[1].text)
                )
            except sqlite3.IntegrityError:
                pass
            
            
            c.execute("""INSERT INTO currencies_values VALUES ('{}', '{}', {}, {})"""
               .format(date, children[0].text, children[2].text, children[3].text)
            )
        
        conn.commit()
        
        time.sleep(1)

    print("iteration")
