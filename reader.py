import argparse
import datetime as dt
from model import Model

model = Model()


def valid_date(s: str):
    try:
        return dt.datetime.strptime(s, "%Y-%m-%d")
        # return True
    except ValueError:
        msg = "'{}' is not a valid date".format(s)
        raise argparse.ArgumentTypeError(msg)


# print(len(list(model.get_missed_dates(100))))
# pprint.pprint(list(model.get_missed_dates(100)))

parser = argparse.ArgumentParser(description="Read data from APB")
subparsers = parser.add_subparsers(help="subparsers")

parser_count = subparsers.add_parser("from_now", help="count of day")
parser_count.add_argument("N", type=int, help="Count of days from now")

parser_period = subparsers.add_parser("period", help="date period")
parser_period.add_argument("DATE_FROM", type=valid_date, help="first date of period")
parser_period.add_argument("DATE_TO", type=valid_date, help="last date of period")

args = parser.parse_args()
print(args)
