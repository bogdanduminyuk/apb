import os
import sqlite3

DATABASE_PATH = os.path.abspath("./cache/currencies.db")


class Database:
    def __init__(self):
        self.conn = sqlite3.connect(DATABASE_PATH)

        self.conn.execute("""
            CREATE TABLE IF NOT EXISTS currencies_list (
                currency_id TEXT PRIMARY KEY,
                currency_name TEXT UNIQUE
            );
        """)

        self.conn.execute("""
            CREATE TABLE IF NOT EXISTS currencies_values (
                date_id TEXT,
                currency_id TEXT NOT NULL,
                buy_value REAL NOT NULL,
                sell_value REAL NOT NULL,
                PRIMARY KEY (date_id, currency_id)
                FOREIGN KEY (currency_id) REFERENCES currencies_list (currency_id)
            );
        """)

        self.c = self.conn.cursor()

    def __del__(self):
        self.conn.commit()
        self.conn.close()

    def add_currency_type(self, currency_id, currency_name):
        query = "INSERT INTO currencies_list VALUES ('{}', '{}')"
        query = query.format(currency_id, currency_name)
        try:
            self.c.execute(query)
            return True
        except sqlite3.IntegrityError:
            return False

    def add_currency_value(self, date_id, currency_id, buy_value, sell_value):
        query = "INSERT INTO currencies_values VALUES ('{}', '{}', {}, {})"
        self.c.execute(query.format(date_id, currency_id, buy_value, sell_value))

    def get_currencies(self):
        self.c.execute("SELECT * FROM currencies_list")
        return self.c.fetchall()

    def __contains__(self, date):
        query = "SELECT date_id FROM currencies_values WHERE DATE(date_id) == DATE('{}')"
        self.c.execute(query.format(date))

        res1 = self.c.fetchall()
        res2 = self.get_currencies()

        return len(res1) == len(res2)
