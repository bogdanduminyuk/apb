import datetime as dt

from .database import Database
from .grab import Grab


class Model:
    def __init__(self):
        self.db = Database()
        self.grab = Grab(self.db)

    def get_missed_dates(self, days_count: int):
        now = dt.datetime.now()

        return (now - dt.timedelta(i) for i in range(days_count) if now - dt.timedelta(i) not in self.db)
