import re
import time

import requests
from bs4 import BeautifulSoup as bs

from model.database import Database

HEADERS = {
    "accept": "text/html, */*; q=0.01",
    "accept-encoding": "gzip, deflate, br",
    "accept-language": "ru,ru-RU;q=0.9,en-US;q=0.8,en;q=0.7,uk;q=0.6",
    "referer": "https://www.agroprombank.com/info/hisrates.html",
    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36",
    "x-requested-with": "XMLHttpRequest",
    "path": "/histrates.php?type=ib&date=30.09.2020",
    "authority": "www.agroprombank.com",
    "scheme": "https"
}


def __bs_preprocess__(html):
    """remove distracting whitespaces and newline characters"""
    pat = re.compile('(^[\s]+)|([\s]+$)', re.MULTILINE)
    html = re.sub(pat, '', html)       # remove leading and trailing whitespaces
    html = re.sub('\n', ' ', html)     # convert newlines to spaces
                                       # this preserves newline delimiters
    html = re.sub('[\s]+<', '<', html) # remove whitespaces before opening tags
    html = re.sub('>[\s]+', '>', html) # remove whitespaces after closing tags
    return html


class Grab:
    def __init__(self, db: Database, sleep_interval: int = 5):
        self.db = db
        self.sleep_interval = sleep_interval

    def get_data(self, dates: list):
        for date in dates:
            params = {
                "type": "ib",
                "date": date.strftime("%d.%m.%Y")
            }

            response = requests.get(
                "https://www.agroprombank.com/histrates.php",
                params=params,
                headers=HEADERS
            )

            if not response.ok:
                raise ValueError

            content = __bs_preprocess__(response.text)
            soup = bs(content, "html.parser")

            tr = soup.find_all("tr")

            for row in tr[1:]:
                children = [child for child in row.children]

                self.db.add_currency_type(children[0].text, children[1].text)
                self.db.add_currency_value(date, children[0].text, children[2].text, children[3].text)

            time.sleep(self.sleep_interval)
